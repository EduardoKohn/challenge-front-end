const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  devtool: "source-map",
  output: {
    filename: "static/js/[name]~[contenthash].js",
  },
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: /(fonts|node_modules)/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: false,
            },
          },
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              config: {
                path: path.resolve(__dirname, "../postcss.config.js"),
              },
            },
          },
          "sass-loader",
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d\.\d\.\d)?$/,
        exclude: /(images|node_modules)/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name]~[contenthash:8].[ext]",
              outputPath: "static/assets/fonts/",
            },
          },
        ],
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        exclude: /(fonts|node_modules)/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name]~[contenthash:8].[ext]",
              outputPath: "static/assets/images/",
            },
          },
        ],
      },
    ],
  },
  optimization: {
    runtimeChunk: "single",
    splitChunks: {
      chunks: "all",
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors", // Enable this key/value pair if you want to publish only one third-party libraries package.
          // name: function (module) {
          //   const packageName = module.context.match(
          //     /[\\/]node_modules[\\/](.*?)([\\/]|$)/
          //   )[1];
          //   return `npm.${packageName.replace("@", "")}`;
          // },
        },
      },
    },
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "static/css/[name]~[contenthash].css",
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, "../public/robots.txt"),
          to: "./robots.txt",
        },
      ],
    }),
  ],
};
