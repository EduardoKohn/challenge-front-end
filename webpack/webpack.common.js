const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const StylelintPlugin = require("stylelint-webpack-plugin");

module.exports = {
  entry: path.resolve(__dirname, "../index.js"),
  output: {
    path: path.resolve(__dirname, "../build"),
    publicPath: "/",
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: { loader: "babel-loader" },
      },
    ],
  },
  resolve: {
    extensions: [".mjs", ".js", ".jsx"],
  },
  plugins: [
    new webpack.HashedModuleIdsPlugin(),
    new HtmlWebpackPlugin({
      template: "public/index.html",
    }),
    new StylelintPlugin({
      options: {
        configFile: path.resolve(__dirname, "stylelint.config.js"),
      },
    }),
    new FaviconsWebpackPlugin({
      // Your source logo (required)
      logo: path.resolve(__dirname, "../src/assets/images/favicon.png"),
      // logo: "../src/assets/images/favicon.png",
      // Path to store cached data or false/null to disable caching altogether
      // Note: disabling caching may increase build times considerably
      cache: "../.wwp-cache",
      // Prefix path for generated assets
      prefix: "static/assets/images/icons/",
      // Favicons configuration options. Read more on: https://github.com/evilebottnawi/favicons#usage
      favicons: {
        appName: "ReactJS Template", // Your application's name. `string`
        appShortName: "ReactJS", // Your application's short name. `string` : Not implemented
        appDescription: "Demo: How to use the ReactJS and webpack plugin", // Your application's description. `string`
        developerName: "eagle-head", // Your (or your developer's) name. `string`
        developerURL: "https://github.com/eagle-head/", // Your (or your developer's) URL. `string`
        dir: "auto", // Primary text direction for name, short_name, and description
        lang: "en-US", // Primary language for name and short_name
        background: "#AAA", // Background colour for flattened icons. `string`
        theme_color: "#BBB", // Theme color user for example in Android's task switcher. `string`
        display: "standalone", // Preferred display mode: "fullscreen", "standalone", "minimal-ui" or "browser". `string`
        appleStatusBarStyle: "black-translucent", // Color for appleStatusBarStyle : Not implemented Not implemented (black-translucent | default | black)
        orientation: "any", // Default orientation: "any", "natural", "portrait" or "landscape". `string`
        start_url: "/?utm_source=homescreen", // Start URL when launching the application from a device. `string`
        scope: ".", // Color for appleStatusBarStyle : Not implemented
        version: "0.0.1", // Your application's version string. `string`
        logging: false, // Print logs to console? `boolean`
        icons: {
          favicons: true, // Create regular favicons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          android: true, // Create Android homescreen icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          appleIcon: false, // Create Apple touch icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          appleStartup: false, // Create Apple startup images. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          coast: false, // Create Opera Coast icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          firefox: false, // Create Firefox OS icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          windows: false, // Create Windows 8 tile icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          yandex: false, // Create Yandex browser icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
        },
      },
    }),
  ],
};
