const merge = require("webpack-merge");
const common = require("./webpack.common");
const development = require("./webpack.development");
const production = require("./webpack.production");

module.exports = function (_, argv) {
  const mode = argv.mode;

  if (mode === "development") {
    return merge([common, development, { mode }]);
  } else {
    return merge([common, production, { mode }]);
  }
};
