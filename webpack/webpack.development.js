const path = require("path");
const webpack = require("webpack");

module.exports = {
  output: {
    filename: "[name]~[hash].js",
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            cacheDirectory: true,
          },
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: /node_modules/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        exclude: /(fonts|node_modules)/,
        use: {
          loader: "url-loader",
          options: {
            limit: 50000,
          },
        },
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /(images|node_modules)/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "assets/fonts/",
            },
          },
        ],
      },
    ],
  },
  devtool: "eval-source-map",
  devServer: {
    host: process.env.HOST, // Defaults to `localhost`
    port: process.env.PORT, // Defaults to 8080
    contentBase: path.resolve(__dirname, "./build"),
    overlay: true, // WDS provides an overlay for capturing compilation related warnings and errors.
    hot: true,
    open: true,
    compress: true,
    watchOptions: {
      aggregateTimeout: 300, // Delay the rebuild after the first change.
      poll: 1000, // Poll using interval (in ms, accepts boolean too).
    },
  },
  plugins: [
    new webpack.WatchIgnorePlugin([path.resolve(__dirname, "node_modules")]),
  ],
};
