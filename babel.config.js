module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        useBuiltIns: "entry",
        corejs: 3,
        targets: {
          esmodules: false,
          node: "current",
        },
      },
    ],
    "@babel/preset-react",
  ],
  env: {
    development: {
      plugins: [
        [
          "@babel/plugin-transform-runtime",
          { corejs: { version: 3, proposals: true } },
        ],
        ["@babel/plugin-syntax-dynamic-import"],
      ],
    },
    production: {
      plugins: ["@babel/plugin-syntax-dynamic-import"],
    },
    test: {
      plugins: ["dynamic-import-node"],
    },
  },
};
