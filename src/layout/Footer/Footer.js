import React from "react";
import Facebook from "../../components/Facebook";
import Twitter from "../../components/Twitter";
import GooglePlus from "../../components/GooglePlus";
import Pinterest from "../../components/Pinterest";

export default function Footer() {
  return (
    <footer id="footer">
      <h1 className="primary-heading footer">Keep in touch with us</h1>
      <p className="paragraph footer">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
        odio augue, porttitor ac pulvinar vel, commodo non nisi. Nullam non dui
        arcu. Vestibulum eget justo non est malesuada ultricies.
      </p>
      <div className="container-submit">
        <input
          type="email"
          name="email"
          id="email"
          placeholder="enter your email to update"
        />
        <a className="btn btn--golden" href="">
          submit
        </a>
      </div>
      <div className="container-social-media">
        <Facebook />
        <Twitter />
        <GooglePlus />
        <Pinterest />
      </div>
      <ul className="address__list">
        <li className="address__list--item">
          <p className="paragraph footer address">Alameda Santos, 1970</p>
          <p className="paragraph footer address">
            6th floor - Jardim Paulista
          </p>
          <p className="paragraph footer address">São Paulo - SP</p>
          <p className="paragraph footer address">+55 11 3090 8500</p>
        </li>
        <li className="address__list--item">
          <p className="paragraph footer address">London - UK</p>
          <p className="paragraph footer address">125 Kingsway</p>
          <p className="paragraph footer address">London WC2B 6NH</p>
        </li>
        <li className="address__list--item">
          <p className="paragraph footer address">Lisbon - Portugal</p>
          <p className="paragraph footer address">Rua Rodrigues Faria, 103</p>
          <p className="paragraph footer address">4th floor</p>
          <p className="paragraph footer address">Lisbon - Portugal</p>
        </li>
        <li className="address__list--item">
          <p className="paragraph footer address">Curitiba - PR</p>
          <p className="paragraph footer address">R. Francisco Rocha, 198</p>
          <p className="paragraph footer address">Batel - Curitiba - PR</p>
        </li>
        <li className="address__list--item">
          <p className="paragraph footer address">Buenos Aires</p>
          <p className="paragraph footer address">Esmeralda 950</p>
          <p className="paragraph footer address">Buenos Iares B C1007</p>
        </li>
      </ul>
    </footer>
  );
}
