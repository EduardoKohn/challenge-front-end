import React from "react";

export default function Main() {
  return (
    <main id="main">
      <h1 className="primary-heading main">Books</h1>
      <p className="paragraph main">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
        odio augue, porttitor ac pulvinar vel, commodo non nisi. Nullam non dui
        arcu. Vestibulum eget justo non est malesuada ultricies.
      </p>
      <div className="container-books">
        <ul className="book__list">
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
          <li className="book__list--item">
            <img
              src="https://books.google.com/books/content?id=DKcWE3WXoj8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
              alt="book"
            />
          </li>
        </ul>
      </div>
    </main>
  );
}
