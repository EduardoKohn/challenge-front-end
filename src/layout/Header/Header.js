import React from "react";
import MobileLoading from "../../components/MobileLoading";
import Apple from "../../components/Apple";
import Android from "../../components/Android";
import Windows from "../../components/Windows";

export default function Header() {
  return (
    <header id="header">
      <nav className="navigation">
        <div className="navigation__logo">
          <div className="navigation__logo--image"></div>
        </div>
        <ul className="navigation__list">
          <li className="navigation__list--item">Books</li>
          <li className="navigation__list--item">Newsletter</li>
          <li className="navigation__list--item">Address</li>
        </ul>
      </nav>
      <div className="navigation__content">
        <div className="navigation__content--left">
          <div className="box">
            <h1 className="primary-heading">Pixter Digital Books</h1>
            <h3 className="tertiary-heading">
              Lorem ipsum dolor sit amet? consectetur adipiscing elit.
            </h3>
            <p className="paragraph">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Pellentesque odio augue, porttitor ac pulvinar vel, commodo non
              nisi. Nullam non dui arcu. Vestibulum eget justo non est malesuada
              ultricies.
            </p>
            <div className="mobile-devices">
              <Apple />
              <Android />
              <Windows />
            </div>
          </div>
          <div className="dots">
            <span className="dot active"></span>
            <span className="dot"></span>
            <span className="dot"></span>
          </div>
        </div>
        <div className="navigation__content--right">
          <MobileLoading />
        </div>
      </div>
    </header>
  );
}
