import React from "react";

export default function MobileLoading() {
  return (
    <div>
      <lottie-player
        src="https://assets5.lottiefiles.com/packages/lf20_ViifFo.json"
        background="transparent"
        speed="1"
        style={{
          maxWidth: "400px",
          maxHeight: "400px",
        }}
        loop
        autoplay
      ></lottie-player>
    </div>
  );
}
