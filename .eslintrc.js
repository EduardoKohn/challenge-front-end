module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2020: true,
    jest: true,
  },
  extends: [
    "plugin:react/recommended",
    "standard",
    "plugin:prettier/recommended",
    "plugin:jest/recommended",
  ],
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    babelOptions: {
      configFile: "./babel.config.js",
    },
    ecmaVersion: 11,
    sourceType: "module",
    allowImportExportEverywhere: true,
    codeFrame: true,
  },
  plugins: ["react", "babel", "prettier", "jest"],
  rules: {},
};
