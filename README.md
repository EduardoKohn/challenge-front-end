# Pixter Technologies

### Criado e desenvoldido por: **Eduardo Kohn**

### Este projeto faz parte do Desafio da Pixter Technologies para vaga de Senior Front End Developer.

---

## Instalando e Rodando

Você precisar ter o `NodeJS` instalado em sua máquina e também o `NPM - Node Packge Management` ( é instalado junto com o Node.js ). Este projeto usou como Biblioteca JavaScript o `ReactJS` e como pré-processador de CSS, o `Sass`.

## Tabela das principais bibliotecas utilizadas

|     Nome      |  Versão   |
| :-----------: | :-------: |
|   **react**   | `16.13.1` |
| **react-dom** | `16.13.1` |
|   **axios**   | `0.19.2`  |
| **node-sass** | `4.14.1`  |
|   **node**    | `12.18.0` |
|    **npm**    | `6.14.4`  |

## Instalação

Após fazer download do repositório rode o seguinte comando no terminal aberto na pasta raíz do projeto:

```sh
npm install
```

## Modo Desenvolvedor

Para rodar o teste em modo desenvolvedor, rode o seguinte código no terminal:

```sh
npm start
```

## Modo de Produção

Para rodar o teste em modo de produção, rode o seguinte código no terminal:

```sh
npm run build
```

## Iniciar Testes

Para rodar os teste, rode o seguinte código no terminal:

```sh
npm test
```
